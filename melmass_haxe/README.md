
## Usage
```bash
# build the image
docker build -t melmass/haxe . 
# run it interactively
docker run -it melmass/haxe sh 
# Public to docker hub where TAG is valid tag
docker push melmass/haxe:TAG 
```